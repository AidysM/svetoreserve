from django.contrib import admin

from .models import Group, Product


admin.site.register(Group)

admin.site.register(Product)
