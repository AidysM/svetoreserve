from django import forms


class JSONInputForm(forms.Form):
    json_input = forms.JSONField()
