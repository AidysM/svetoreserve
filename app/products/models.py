from django.db import models
from django.contrib.auth.models import User


class Group(models.Model):
    category = models.CharField(max_length=256)

    def __str__(self):
        return f'{self.category}'


class Product(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE, default=1)
    name = models.CharField(max_length=256)
    image = models.ImageField(blank=True)
    count = models.IntegerField(default=1)
    price = models.DecimalField(max_digits=10, decimal_places=2, default='0.00')
    description = models.TextField()
    other = models.TextField(blank=True)

    def __str__(self):
        return f'{self.name}'


class UserRequest(models.Model):
    req_user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=64)


class Page(models.Model):
    userrequest = models.ForeignKey(UserRequest, on_delete=models.CASCADE)
    url = models.URLField(blank=True)


