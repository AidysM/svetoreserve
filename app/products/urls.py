from django.urls import path, include
from rest_framework import permissions
from rest_framework.routers import DefaultRouter

from .views import index, GroupViewSet, ProductViewSet

urlpatterns = [
    path('', index, name='index'),
]

router = DefaultRouter()

router.register('groups', GroupViewSet)
router.register('api/v1', ProductViewSet)

urlpatterns += router.urls

