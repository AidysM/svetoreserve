import json
import re

from django.shortcuts import render
from rest_framework import viewsets, permissions
from bs4 import BeautifulSoup
import requests

from .serializers import GroupSerializer, ProductSerializer
from .models import Group, Product
from .forms import JSONInputForm


class GroupViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = GroupSerializer
    queryset = Group.objects.all()


class ProductViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ProductSerializer
    queryset = Product.objects.all()


# json_input = '{"urls":["https://www.top-shop.ru/"]}'

def index(request):
    submitbutton = request.POST.get('submit')

    jsonfield = {}
    urls = []

    form = JSONInputForm(request.POST or None)
    if form.is_valid():
        jsonfield_ = form.cleaned_data.get('json_input')
        data_dct = json.dumps(jsonfield_)
        jsonfield = json.loads(data_dct)
        # print(type(data_dct))
        # print(data_dct[:100])
    # urls = []
    if jsonfield.get('urls'):
        urls = jsonfield.get('urls')
        # print(urls)

    links = []
    cat_names = []
    prod_names = []

    obshaya = Group.objects.filter(id=1)
    if obshaya.exists():
        obshaya.delete()
    else:
        Group.objects.create(id=1, category='Общая')

    if urls:
        for url in urls:
            # print(url)
            r = requests.get(url)
            soup = BeautifulSoup(r.content, 'html.parser')
            # html = soup.find('html')
            # body = soup.find('body')
            words = ['Категория', 'категория', 'Категории', 'категории',
                     'Товары', 'товары', 'Товар', 'товар', ]
            cat_links = []
            prod_links = []
            for word in words:
                links.append(soup.findAll('a', string=re.compile(word)))
                if word[0] == 'К' or word[0] == 'к':
                    categories = soup.findAll('a', string=re.compile(word))
                    for cat in categories:
                        cat_links.append(cat.contents)

                elif word[0] == 'Т' or word[0] == 'т':
                    products = soup.findAll('a', string=re.compile(word))
                    # print(products.contents)
                    for prod in products:
                        prod_links.append(prod.string)

            # print(links[:10])
            prod_names = list(set(prod_links))
            cat_names = list(set(cat_links))

            for cat_name in cat_names:
                Group.objects.create(category=cat_name)
                # print(cat_name)

            for prod_name in prod_names:
                Product.objects.create(name=prod_name)
                # print(prod_name)

    context = {'form': form, 'jsonfield': jsonfield, 'submitbutton': submitbutton, 'urls': urls,
               'links': links, 'cat_names': cat_names, 'prod_names': prod_names}

    return render(request, 'index.html', context)
